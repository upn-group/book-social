const URL_BASE = 'https://localhost:44346';


describe('Home Carga Correctamente', () => {  
  it('Home Cargo', () => {    

    cy.request({
    	method: 'GET',
    	url: URL_BASE
    }).then((resp) => {
    	expect(resp.status).to.equal(200);
    })

  })

})


describe('Login satisfactorio', () => {  
  it('Inicio de sesión correcto', () => {    

    cy.request({
    	method: 'post',
    	url: URL_BASE + '/auth/login',
    	form: true,
    	body: {
    		username: 'pepito',
    		password: '123'
    	},
    	failOnStatusCode: false,
    }).then((resp) => {
    	expect(resp.status).to.equal(200);
    	// console.log(resp);
    	// expect(resp.redirects[0].split(': ')[1]).to.equal(URL_BASE + '/');
    })

  })

})


describe('Login incorrecto', () => {  
  it('Inicio de sesión valida Correctamente', () => {    

    cy.request({
    	method: 'post',
    	url: URL_BASE + '/auth/login',
    	form: true,
    	body: {
    		username: 'pepito',
    		password: '1234'
    	},
    	failOnStatusCode: false,
    }).then((resp) => {
    	expect(resp.status).to.equal(400);
    	
    })

  })

})