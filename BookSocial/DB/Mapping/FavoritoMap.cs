﻿using BookSocial.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookSocial.DB.Mapping
{
    public class FavoritoMap : IEntityTypeConfiguration<Favorito>
    {
        public void Configure(EntityTypeBuilder<Favorito> builder)
        {
            builder.ToTable("Favorito");
            builder.HasKey(o => o.Id);          
        }
    }
}
