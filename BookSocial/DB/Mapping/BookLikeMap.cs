﻿using BookSocial.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookSocial.DB.Mapping
{
    public class BookLikeMap : IEntityTypeConfiguration<BookLike>
    {
        public void Configure(EntityTypeBuilder<BookLike> builder)
        {
            builder.ToTable("BookLike");
            builder.HasKey(book => book.Id);
        }
    }
}
