﻿using BookSocial.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookSocial.DB.Mapping
{
    public class AutorMap : IEntityTypeConfiguration<Autor>
    {
        public void Configure(EntityTypeBuilder<Autor> builder)
        {
            builder.ToTable("Autor");
            builder.HasKey(o => o.Id);

            //builder.HasMany(o => o.Books)
            //    .WithOne(o => o.Autor)
            //    .HasForeignKey(o => o.AutorId);
        }
    }
}
