﻿using BookSocial.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookSocial.DB.Mapping
{
    public class BookMap : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder.ToTable("Book");
            builder.HasKey(book => book.Id);

            //builder.HasOne(o => o.Autor)
            //    .WithMany()
            //    .HasForeignKey(o => o.AutorId);
            
            builder.HasMany(o => o.LibroAutores)
                .WithOne()
                .HasForeignKey(o => o.LibroId);

            builder.HasMany(o => o.Likes)
                .WithOne()
                .HasForeignKey(o => o.BookId);
            
            //builder.HasMany(o => o.Autores)
            //    .WithMany(o => o.Books)
            //    .UsingEntity(o => o.ToTable("LibroAutor"));
        }
    }
}
