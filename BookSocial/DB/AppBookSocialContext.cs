﻿using BookSocial.DB.Mapping;
using BookSocial.Models;
using Microsoft.EntityFrameworkCore;

namespace BookSocial.DB
{
    public class AppBookSocialContext: DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Autor> Autors { get; set; }
        public DbSet<Comentario> Comentarios { get; set; }
        public DbSet<Favorito> Favoritos { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<BookLike> BookLikes { get; set; }

        public AppBookSocialContext(DbContextOptions<AppBookSocialContext> options): base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new AutorMap());
            modelBuilder.ApplyConfiguration(new BookMap());            
            modelBuilder.ApplyConfiguration(new ComentarioMap());
            modelBuilder.ApplyConfiguration(new FavoritoMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new BookLikeMap());
        }
    }
}
