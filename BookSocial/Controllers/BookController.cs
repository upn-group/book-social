﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookSocial.DB;
using BookSocial.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookSocial.Controllers
{ 
    public class BookDetalle {
        public Book Book { get; set; }
        public List<Comentario> Comentarios { get; set; }
    }

    public class BookController : Controller
    {
        private AppBookSocialContext context;
        public BookController(AppBookSocialContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public ViewResult Detalle(int id)
        {
            var books = context.Books;
            
            Book book = books.FirstOrDefault(item => item.Id == id);
            List<Comentario> comentarios = context.Comentarios.Where(o => o.BookId == id).ToList(); // transformo el dbset a lista

            // context.Comentarios.ToList(); => todos los elementos
            // context.Comentarios.Where(o => o.BookId == id).ToList(); => Aplica un where a mi table
            // context.Books.Where(o => o.Nombre.Contains("a")).ToList(); => Aplica un where a mi table

            var detalle = new BookDetalle
            {
                Book = book,
                Comentarios = comentarios
            };

            return View("Detalle", detalle);
        }

        [HttpPost]
        public IActionResult AddComentario(Comentario comentario)
        {
            // comentario.Detalle = null;
            // si comentario no sea null and comentario.Detalle.Length < 5   => lanzar una exception
            //var ultimoComentario = context.Comentarios.OrderBy(o => o.Id).Last();

            //if (ultimoComentario.Detalle != comentario.Detalle)
            //    ModelState.AddModelError("Detalle", "Tu comentario debe ser igual al ultimo ingresado");
           

            if (ModelState.IsValid) // no hay mensajes => 0 mensaje
            {
                context.Comentarios.Add(comentario);
                context.SaveChanges();
                return RedirectToAction("Detalle", new { id = comentario.BookId });
            }
            else
            {
                return Detalle(comentario.BookId);
            }
        }

        [HttpPost]
        [Authorize]
        public IActionResult Favorito(int id)
        {            
            var user = GetLoggedUser();

            context.Favoritos.Add(new Favorito { UsuarioId = user.Id, LibroId = id });
            context.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [Authorize]
        public int Like(int id)
        {
            var user = GetLoggedUser();
            context.BookLikes.Add(new BookLike { BookId = id, UsuarioId = user.Id });
            context.SaveChanges();

            return context.BookLikes.Where(o => o.BookId == id).Count();
        }

        private Usuario GetLoggedUser()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = context.Usuarios.First(o => o.Username == username);
            return user;
        }
    }
}
