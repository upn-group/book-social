﻿using BookSocial.DB;
using BookSocial.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BookSocial.Controllers
{
    [Authorize]
    public class AdminBookController : Controller
    {
        private IWebHostEnvironment hosting;
        private AppBookSocialContext context;

        public AdminBookController(AppBookSocialContext context, IWebHostEnvironment hosting)
        {
            this.hosting = hosting;
            this.context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Autores = context.Autors.ToList();
            return View(new Book());
        }

        [HttpPost]
        public IActionResult Create(Book book, List<IFormFile> files)
        {
            if (ModelState.IsValid)
            {
                book.Caratula = SaveFile(files[0]);

                context.Books.Add(book);
                context.SaveChanges();
               

                return RedirectToAction("Create");
            }

            ViewBag.Autores = context.Autors.ToList();
            return View(book);
        }



        [HttpGet]
        public IActionResult Edit(int id)
        {
            var book = context.Books.Find(id);
            return View(book);
        }

        [HttpPost]
        public IActionResult Edit(Book book, IFormFile file) // new Book {} con los datos nuevos
        {
            //book.Nombre = "este nombre tambien quiereo que se guarade"; // esto no hace nada en la base de datos

            var book_db = context.Books.Find(book.Id); // new Book {} -> conexión con la base de datos
            book_db.Nombre = book.Nombre;
            book_db.AutorId = book.AutorId;
            //context.Books.Update(book); // asegurse de enviar todos los datos
            context.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var book = context.Books.Find(id);
            context.Books.Remove(book);
            
            context.SaveChanges();

            return RedirectToAction("Index");
        }


        private string SaveFile(IFormFile file)
        {
            string relativePath = "";

            if (file.Length > 0 && file.ContentType == "image/png")
            {                
                relativePath = Path.Combine("files", file.FileName);
                var filePath = Path.Combine(hosting.WebRootPath, relativePath);
                var stream = new FileStream(filePath, FileMode.Create);
                file.CopyTo(stream);
                stream.Close();
            }

            return "/" + relativePath.Replace('\\', '/');
        }
    }
}
