﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookSocial.Models
{
    public class BookLike
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        public int UsuarioId { get; set; }
    }
}
