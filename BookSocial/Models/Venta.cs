﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookSocial.Models
{
    public class Venta
    {
        public int Id { get; set; }
        public int CompradorId { get; set; }
        public DateTime FechaVenta { get; set; }
        public List<VentaLibro> Libros { get; set; }
    }
}
