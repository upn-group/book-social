﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookSocial.Models
{
    public class LibroAutor
    {
        public int Id { set; get; }
        public int LibroId { set; get; }
        public int AutorId { set; get; }
        public Autor Autor { get; set; }
    }
}
